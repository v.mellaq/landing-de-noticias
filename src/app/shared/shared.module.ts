import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CoreModule } from '../core/core.module'
import { NavbarComponent } from './components/navbar/navbar.component'
import { ListadoNoticiasComponent } from './components/listado-noticias/listado-noticias.component'
import { FormularioComponent } from './components/formulario/formulario.component'
@NgModule({
  declarations: [
    SpinnerComponent,
    NavbarComponent,
    ListadoNoticiasComponent,
    FormularioComponent
  ],
  imports: [
    CommonModule,
    NgSelectModule,
    ReactiveFormsModule,
    FormsModule,
    CoreModule
  ],
  exports: [
    CommonModule,
    NgSelectModule,
    SpinnerComponent,
    NavbarComponent,
    ListadoNoticiasComponent,
    FormularioComponent,
    ReactiveFormsModule,
    FormsModule
  ],
})
export class SharedModule { }
