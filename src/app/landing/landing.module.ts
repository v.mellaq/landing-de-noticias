import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing.component';
import { SharedModule } from '../shared/shared.module'
import { LandingRoutingModule } from './landing-routing.module'
import { CoreModule } from '../core/core.module'

@NgModule({
  declarations: [LandingComponent],
  imports: [
    CommonModule,
    SharedModule,
    LandingRoutingModule,
    CoreModule
  ]
})
export class LandingModule { }
