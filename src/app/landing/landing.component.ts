import { Component, OnInit } from '@angular/core'
import { IDtoBusqueda } from '../core/interfaces/dto-busqueda.interface'
import { INoticias } from '../core/interfaces/noticias.intreface'
import { NoticiaService } from '../core/services/noticia.service'

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  titulo: string
  lsnoticias: Array<INoticias>
  loading: boolean

  constructor(private noticiaService: NoticiaService) {
    this.titulo = 'Buscador de noticias'
    this.loading = false
    this.lsnoticias = []
  }

  ngOnInit() {
  }

  onHandleBusquedaSeleccionada(evento: IDtoBusqueda) {
    this.loading = true
    this.noticiaService.getNoticias(evento)
      .subscribe((resp: INoticias[]) => {
        this.lsnoticias = resp
        this.loading = false
      })
  }

}
